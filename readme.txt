The Report module provides an overview of user and content activity on your site.

Historical data is collected through a daily cron job, and can be viewed in Daily/Weekly/Monthly/Annual ranges.  The Chart module (http://drupal.org/project/chart) will be used, if available.

Two permissions are available: "access report" and "administer report".

After enabling the Report, go to "http://www.example.com/report" to see the report (Replace the example domain with your own). The links are also added to the navigation block.

You can go to "http://www.example.com/report/settings" to set the time when daily historical data is recorded and other configurations for user and content.